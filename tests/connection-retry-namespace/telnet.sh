#/bin/bash

set -e

DUMMYSYS_ROOT="$1"
LAVA_TESTS_ROOT="$2"

STATE_FILE="/root/connect-device.statefile"
COMMAND_FILE="failure-commands.yaml"

if [ -e "$STATE_FILE" ]
then
  COMMAND_FILE="commands.yaml"
fi

touch "$STATE_FILE"
exec "$DUMMYSYS_ROOT/lava-board" --delay 0 --jitter 0 --command "$LAVA_TESTS_ROOT/$COMMAND_FILE" --context "$LAVA_TESTS_ROOT/context.yaml"

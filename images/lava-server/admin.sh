#!/bin/bash

lava-server manage shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')"
lava-server manage tokens add --user admin --secret admin

